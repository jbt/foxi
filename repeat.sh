#!/bin/bash
cd `dirname "${0}"`
( kate in <&- 2>/dev/null >/dev/null & ) &
t=0
f=`mktemp`
if [ ! -s in ]
then
  echo '    _' > in
fi
until read -N 1 -t 1
do
  if [ ! -s in ]
  then
    break
  elif diff in "${f}"
  then
    #set -x
    inotifywait --quiet --timeout $[++t] -e modify -e attrib -e close_write -e move_self in || echo -n $[++t]
    set +x
  else
    cargo run
    cp -v in "${f}"
    echo $[t /= 2]
  fi
done

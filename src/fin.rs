use crate::err;
use ascii::{AsAsciiStr, AsciiChar, AsciiStr, AsciiString};
use itertools::Itertools;
use std::{
    fs::File,
    io::{BufRead, BufReader, Write},
};

#[derive(Default, Debug)]
pub struct Put {
    pub words: Vec<AsciiString>,
    pub guesses: AsciiString,
}

impl Put {
    pub fn read() -> err::Result<Self> {
        let mut result = Self::default();
        let f = File::open("in")?;
        let br = BufReader::new(f);
        for line in br.lines() {
            let mut line = line?.as_ascii_str()?.to_ascii_string();
            if line.is_empty() {
            } else if result.guesses(&line) {
                for ch in line.chars_mut() {
                    if *ch == AsciiChar::UnderScore {
                        *ch = AsciiChar::Space;
                    }
                }
                result.words.push(line);
            } else if line[0] != '+' {
                println!("Treating {line} as a line of misses.");
            }
        }
        result.guesses = result.guesses.chars().sorted().unique().collect();
        Ok(result)
    }
    fn guesses(&mut self, s: &AsciiStr) -> bool {
        let mut miss = false;
        for ch in s.chars() {
            if ch.is_ascii_lowercase() {
                self.guesses.push(ch);
            } else if ch != ' ' && ch != '_' {
                miss = true;
            }
        }
        s.len() <= 6 && !miss
    }
    pub fn word_len(&self) -> u8 {
        self.words
            .iter()
            .map(|w| w.len())
            .max()
            .expect("There are no words entered.")
            .try_into()
            .expect("Code logic error")
    }
    pub fn complete_words(&self) -> Vec<AsciiString> {
        let l = self.word_len() as usize;
        self.words
            .iter()
            .filter_map(|w| {
                if w.len() == l && !w.as_str().contains(' ') {
                    Some(w.clone())
                } else {
                    None
                }
            })
            .collect()
    }
    pub fn mark_done(&self) {
        let mut f = File::create("in").expect("truncate & open in");
        let l = self.word_len() as usize;
        write!(
            f,
            ".{}\n",
            self.guesses
                .chars()
                .map(|ch| ch.as_char())
                .collect::<String>()
        )
        .unwrap();
        for w in &self.words {
            if w.len() == l && !w.as_str().contains(' ') {
                write!(f, "{}!\n", w).unwrap();
            } else {
                let mut x = w.clone();
                while x.len() < l {
                    x.push(AsciiChar::Space);
                }
                if x[l - 1] == AsciiChar::Space {
                    x[l - 1] = AsciiChar::UnderScore;
                }
                write!(f, "{}\n", x).unwrap();
            }
        }
    }
    pub fn blank(&self) -> AsciiString {
        std::iter::repeat(AsciiChar::Space)
            .take(self.word_len().into())
            .collect()
    }
}

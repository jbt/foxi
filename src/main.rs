mod dict;
mod err;
mod fin;

use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use ascii::{AsAsciiStr, AsciiChar, AsciiString};

fn main() -> err::Result<()> {
    let inp = fin::Put::read().expect("bad input");
    println!("Looking for words of length {}", inp.word_len());
    let mut d = dict::Dict::new(inp.word_len().into())?;
    let mut counts = inp
        .words
        .iter()
        .map(|w| d.count_letters(w, &inp.guesses))
        .reduce(dict::Probs::or)
        .expect("No words?");
    let mut prior = d.count_letters(&inp.blank(), &AsciiString::default());
    for ch in inp.guesses.chars() {
        prior.impossible(ch);
    }
    inp.mark_done();
    if let Ok(add) = adds(inp.word_len()) {
        d.update(add)?;
    } else {
        d.update(inp.complete_words())?;
    }
    recommend(counts.clone());
    if counts.good() || counts.max() > 0.98 {
        return Ok(());
    }
    let top = counts.top_rec();
    while counts.max() < 0.91 && (top == counts.top_rec() || !counts.good()) {
        counts = counts.or(prior.clone());
    }
    recommend(counts.clone());
    Ok(())
}

fn adds(len: u8) -> err::Result<Vec<AsciiString>> {
    let f = File::open(format!("add.{len}"))?;
    let br = BufReader::new(f);
    let mut words = Vec::new();
    for line in br.lines() {
        words.push(line?.as_ascii_str()?.into());
    }
    Ok(words)
}

fn recommend(counts: crate::dict::Probs) {
    let recs: Vec<(f64, AsciiChar)> = counts
        .probs()
        .into_iter()
        .enumerate()
        .map(|(i, f)| (f, AsciiChar::from_ascii(i as u8 + 0x61).unwrap()))
        .collect();
    let mut recs: Vec<(u16, AsciiChar)> = recs
        .into_iter()
        .map(|(f, lt)| (((f * 1000.0) as u16), lt))
        .collect();
    recs.sort();
    println!("Recommendations: ");
    for rec in &recs[22..] {
        println!("  {} : {}.{}%", rec.1, rec.0 / 10, rec.0 % 10);
    }
}

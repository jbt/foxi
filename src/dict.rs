use crate::err::Result;
use ascii::{AsAsciiStr, AsciiChar, AsciiStr, AsciiString};
use itertools::Itertools;
use std::io::{BufRead, Write};
use std::{fs::File, io::BufReader, str::FromStr};

pub struct Dict {
    entries: Vec<(AsciiString, u16)>,
}

impl Dict {
    pub fn new(word_len: u16) -> Result<Self> {
        let f = File::open(word_len.to_string())?;
        let br = BufReader::new(f);
        let mut result = Self {
            entries: Vec::default(),
        };
        let xfrm = |l: &str| {
            (
                l[0..word_len.into()].to_string(),
                u16::from_str(&l[word_len.into()..]),
            )
        };
        for line in br.lines() {
            let (key, val) = xfrm(&line?);
            result
                .entries
                .push((key.as_ascii_str()?.to_ascii_string(), val?));
        }
        Ok(result)
    }
    pub fn count_letters(&self, pat: &AsciiStr, g: &AsciiString) -> Probs {
        let mut n = 0.0;
        let mut fs = [0f64; 26];
        for cand in self.entries.iter().filter(|(w, _)| patmat(pat, w, g)) {
            n += cand.1 as f64;
            // println!("{:?} matches, n={}", &cand, n);
            for ch in cand.0.chars().unique() {
                if !g.chars().contains(&ch) {
                    // println!("Adding {} for {}", cand.1, ch);
                    fs[(ch.as_byte() - 0x61) as usize] += cand.1 as f64;
                }
            }
        }
        let mut result = Probs::default();
        for i in 0..26 {
            result.arr[i] = (255. * fs[i] / n) as u8;
        }
        result
    }
    pub fn update(&mut self, up_words: Vec<AsciiString>) -> Result<()> {
        if up_words.is_empty() {
            return Ok(());
        }
        let mut word_len = 5;
        for u in up_words {
            if u.len() == 6 {
                word_len = 6;
            } else if u.len() != word_len {
                println!("Rejecting {u} as the wrong length of word!");
                continue;
            }
            match self.entries.binary_search_by(|(w, _)| w.cmp(&u)) {
                Ok(index) => {
                    let e = &mut self.entries[index];
                    println!("Upping {} from {} to {}", e.0, e.1, e.1 + 1);
                    e.1 += 1;
                }
                Err(index) => {
                    println!("Inserting {} at {}", u, index);
                    self.entries.insert(index, (u, 1));
                }
            }
        }
        println!("writing to file word_len={word_len}");
        let mut f = File::create(word_len.to_string())?;
        self.entries.sort();
        for e in &self.entries {
            write!(f, "{}{}\n", e.0, e.1)?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, Default)]
pub struct Probs {
    pub arr: [u8; 26],
}

impl Probs {
    pub fn or(self, rhs: Self) -> Self {
        let mut result = Self { arr: [0; 26] };
        for i in 0..26 {
            let na: u16 = (0xFF - self.arr[i]).into();
            let nb: u16 = (0xFF - rhs.arr[i]).into();
            let n = (na * nb) / 0xFF;
            /*
            println!(
                "a={},b={},~a={},~b={},~a ^ ~b={}, not that {}",
                self.arr[i],
                rhs.arr[i],
                na,
                nb,
                n,
                256 - n
            );
             */
            result.arr[i] = (256 - n).clamp(0, 0xFF) as u8;
        }
        result
    }
    pub fn max(&self) -> f64 {
        (*self.arr.iter().max().unwrap_or(&0) as f64) / 255.
    }
    pub fn probs(&self) -> [f64; 26] {
        let mut result = [0f64; 26];
        for i in 0..26 {
            result[i] = self.arr[i] as f64 / 255.;
        }
        result
    }
    pub fn impossible(&mut self, ch: AsciiChar) {
        self.arr[(ch.as_byte() - 0x61) as usize] = 0;
    }
    pub fn good(&self) -> bool {
        let mut t = [0u8;2];
        for b in self.arr {
            if b > t[0] {
                t[0] = b;
                t[1] = 1;
            } else if b == t[0] {
                t[1] += 1;
            }
        }
        t[0] > 129 && t[1] == 1
    }
    pub fn top_rec(&self) -> u8 {
        let mut w = 0;
        let mut m = 0;
        for (i,p) in self.arr.iter().enumerate() {
            if *p > m {
                m = *p;
                w = i;
            }
        }
        w as u8
    }
}

fn patmat(p: &AsciiStr, w: &AsciiStr, g: &AsciiString) -> bool {
    for i in 0..w.len() {
        if i >= p.len() || p[i] == ' ' {
            if g.chars().contains(&w[i]) {
                return false;
            }
        } else if p[i] != w[i] {
            return false;
        }
    }
    true
}

use derive_error::Error;

#[derive(Error,Debug)]
pub enum Generic {
    Io(std::io::Error),
    NonIntArg(std::num::ParseIntError),
    NonAscii(ascii::AsAsciiStrError),
}

pub type Result<T> = std::result::Result<T,Generic>;
